# OPEN ESB COMPONENT 3 #

## Trouble Shooting ##

### Problem accessing OpenESB Community Nexus because ###

`Could not transfer artifact XXXX:YYYY from/to nexus-community-stam (https://nexus.open-esb.net/repository/maven-releases/): sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target`

This is because our server uses Let's Encrypt root certificates which is not in older versions of Java. You need to 
download the [chain.pem](stuff/ssl/chain.pem) file import it to your 
trust store using keytool

`<JAVA_HOME>/jre/bin/keytool -trustcacerts -keystore <JAVA_HOME>/jre/lib/security/cacerts -storepass changeit -noprompt -importcert -file <DOWNLOAD_FOLDER>/chain.pem `