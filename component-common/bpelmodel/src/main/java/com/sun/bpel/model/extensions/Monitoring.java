/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 *   
 * 2020 Pymma Consulting TLD 
 */
package com.sun.bpel.model.extensions;

import com.sun.bpel.model.From;
import com.sun.bpel.xml.common.model.XMLElement;

/*
 * @(#)Monitoring.java 
 *
 * Copyright 2019*-2020 Pymma consulting, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
public interface Monitoring extends XMLElement {

    /**
     * Tag for this element.
     */
    public static final String TAG = "monitoring";

    /**
     * Describes attributes for this element.
     */
    public interface ATTR {

        /**
         * "level" attribute token
         */
        public static final String LEVEL = "level";

        /**
         * "location" attribute token
         */
        public static final String LOCATION = "location";
    }

    /**
     * Ordinal position for level attribute
     */
    public static final int LEVEL = 0;

    /**
     * Ordinal position for location attribute
     */
    public static final int LOCATION = LEVEL + 1;

    /**
     *      */
    public static final String CHANNEL01 = "channel01";

    /**
     *      */
    public static final String CHANNEL02 = "channel02";

    /**
     *      */
    public static final String CHANNEL03 = "channel03";

    /**
     *      */
    public static final String CHANNEL04 = "channel04";

    /**
     *      */
    public static final String CHANNEL05 = "channel05";

    /**
     *      */
    public static final String CHANNEL06 = "channel06";

    /**
     *      */
    public static final String CHANNEL07 = "channel07";

    /**
     *      */
    public static final String DEFAULT_LEVEL = CHANNEL01;

    /**
     *      */
    public static final String DEFAULT_LOCATION = Trace.ON_COMPLETE;

    /**
     *
     * @param level
     */
    public void setLevel(String level);

    /**
     *
     * @param location
     */
    public void setLocation(String location);

    /**
     *
     * @param from
     */
    public void setFrom(From from);

    /**
     *
     * @return
     */
    public String getLevel();

    /**
     *
     * @return
     */
    public String getLocation();

    /**
     *
     * @return
     */
    public From getFrom();

}
