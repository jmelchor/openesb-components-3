/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */
package com.sun.bpel.model.extensions.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.xml.namespace.QName;

import com.sun.bpel.model.extensions.Alert;
import com.sun.bpel.model.extensions.Log;
import com.sun.bpel.model.extensions.Monitoring;
import com.sun.bpel.model.extensions.Trace;
import com.sun.bpel.model.impl.BPELElementImpl;
import com.sun.bpel.model.visitor.BPELVisitor;
import com.sun.bpel.xml.NamespaceUtility;
import com.sun.bpel.xml.common.model.XMLDocument;
import com.sun.bpel.xml.common.model.XMLElement;
import com.sun.bpel.xml.common.visitor.Visitor;

/*
 * @(#)TraceImpl.java 
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * Copyright 2019-2020 Pymma Consulting. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
public class TraceImpl extends BPELElementImpl implements Trace {

    /* */
    private static final long serialVersionUID = -5467222970905881239L;

    /**
     * QName object for SeeBeyond Private extension line label
     */
    public static final QName TRACE_QNAME = NamespaceUtility.getQName(
            XMLElement.SBYNBPEL_RUNTIME_EXTN_NAMESPACE, Trace.TAG, XMLElement.SBYNBPEL_RUNTIME_EXTN_PREFIX);

    private ArrayList<Log> mLogs = new ArrayList<>();

    private ArrayList<Log> mOnStartLogs = new ArrayList<>();

    private ArrayList<Log> mOnCompleteLogs = new ArrayList<>();
    
    private ArrayList<Monitoring> mMonitorings = new ArrayList<>();

    private ArrayList<Monitoring> mOnStartMonitorings = new ArrayList<>();

    private ArrayList<Monitoring> mOnCompleteMonitorings = new ArrayList<>();

    private ArrayList<Alert> mAlerts = new ArrayList<Alert>();

    private ArrayList<Alert> mOnStartAlerts = new ArrayList<Alert>();

    private ArrayList<Alert> mOnCompleteAlerts = new ArrayList<Alert>();

    /**
     * Creates a new instance of TraceImpl
     */
    public TraceImpl() {
        super();
        initTrace();
    }

    /**
     * Creates a new instance of TraceImpl.
     *
     * @param d Owner document.
     */
    public TraceImpl(XMLDocument d) {
        super(d);
        initTrace();
    }

    /**
     * Initializes this class.
     */
    private void initTrace() {
        setLocalName(Trace.TAG);
        setQualifiedName(TRACE_QNAME);
        childrenTags = new String[]{
            Log.TAG,
            Alert.TAG,
            Monitoring.TAG    
        };
    }

    /**
     * @param w
     * 
     * @see XMLNode#accept
     */
    @Override
    public boolean accept(Visitor w) {
        BPELVisitor v = morphVisitor(w);
        if (traverseParentFirst(v)) {
            if (!v.visit(this)) {
                return false;
            }
        }

        if (!super.accept(v)) {
            return false;
        }

        if (traverseParentLast(v)) {
            if (!v.visit(this)) {
                return false;
            }
        }
        return true;
    }

    /* (non-Javadoc)
     * @see com.sun.bpel.model.extensions.Trace#addAlert(com.sun.bpel.model.extensions.Alert)
     *
     * @param alert
     */
    @Override
    public synchronized void addAlert(Alert alert) {
        super.addChild(2, alert);
        mAlerts.add(alert);
        if (alert.getLocation().equals(Trace.ON_START)) {
            mOnStartAlerts.add(alert);
        } else {
            mOnCompleteAlerts.add(alert);
        }
    }

    /* (non-Javadoc)
	 * @see com.sun.bpel.model.extensions.Trace#addLog(com.sun.bpel.model.extensions.Log)
     */

    /**
     *
     * @param log
     */
    @Override
    public synchronized void addLog(Log log) {
        super.addChild(1, log);
        mLogs.add(log);
        if (log.getLocation().equals(Trace.ON_START)) {
            mOnStartLogs.add(log);
        } else {
            mOnCompleteLogs.add(log);
        }
    }
    
    @Override
    public synchronized void addMonitoring(Monitoring monitoring) {
        super.addChild(1, monitoring);
        mMonitorings.add(monitoring);
        if (monitoring.getLocation().equals(Trace.ON_START)) {
            mOnStartMonitorings.add(monitoring);
        } else {
            mOnCompleteMonitorings.add(monitoring);
        }
    }

    /* (non-Javadoc)
	 * @see com.sun.bpel.model.extensions.Trace#clearAlerts()
     */
    @Override
    public synchronized void clearAlerts() {

        for (int i = 0; i < mAlerts.size(); i++) {
            super.removeChild(mAlerts.get(i));
        }

        mAlerts.clear();
        mOnStartAlerts.clear();
        mOnCompleteAlerts.clear();
    }

    /* (non-Javadoc)
	 * @see com.sun.bpel.model.extensions.Trace#clearLogs()
     */
    @Override
    public synchronized void clearLogs() {

        for (int i = 0; i < mLogs.size(); i++) {
            super.removeChild(mLogs.get(i));
        }
        mLogs.clear();
        mOnStartLogs.clear();
        mOnCompleteLogs.clear();
    }

   @Override
    public synchronized void clearMonitorings() {
        for (int i = 0; i < mMonitorings.size(); i++) {
            super.removeChild(mMonitorings.get(i));
        }
        mMonitorings.clear();
        mOnStartMonitorings.clear();
        mOnCompleteMonitorings.clear();
    } 
    
    
    @Override
    public int getAlertSize() {
        return mAlerts.size();
    }

    
    @Override
    public int getLogSize() {
        return mLogs.size();
    }
    
    @Override
    public int getMonitoringSize() {
        return mMonitorings.size();
    }
    
    
    @Override
    public synchronized Collection<Alert> getAlerts() {
        return Collections.unmodifiableCollection((ArrayList<? extends Alert>) mAlerts.clone());
    }

    @Override
    public synchronized Collection<Log> getLogs() {
        return Collections.unmodifiableCollection((ArrayList<? extends Log>) mLogs.clone());
    }

    @Override
    public synchronized Collection<Monitoring> getMonitorings() {
        return Collections.unmodifiableCollection((ArrayList<? extends Monitoring>) mMonitorings.clone());
    }
    
    
    @Override
    public synchronized Collection<Alert> getOnCompleteAlerts() {
        return Collections.unmodifiableCollection((ArrayList<? extends Alert>) mOnCompleteAlerts.clone());
    }

    @Override
    public synchronized Collection<Log> getOnCompleteLogs() {
        return Collections.unmodifiableCollection((ArrayList<? extends Log>) mOnCompleteLogs.clone());
    }
    
    @Override
    public synchronized Collection<Monitoring> getOnCompleteMonitorings() {
        return Collections.unmodifiableCollection((ArrayList<? extends Monitoring>) mOnCompleteMonitorings.clone());
    }

    @Override
    public synchronized Collection<Alert> getOnStartAlerts() {
        return Collections.unmodifiableCollection((ArrayList<? extends Alert>) mOnStartAlerts.clone());
    }

    @Override
    public synchronized Collection<Log> getOnStartLogs() {
        return Collections.unmodifiableCollection((ArrayList<? extends Log>) mOnStartLogs.clone());
    }
    
    @Override
    public synchronized Collection<Monitoring> getOnStartMonitorings() {
        return Collections.unmodifiableCollection((ArrayList<? extends Monitoring>) mOnStartMonitorings.clone());
    }

    
    @Override
    public boolean hasAlerts() {
        return (mAlerts.size() > 0);
    }

    @Override
    public boolean hasLogs() {
        return (mLogs.size() > 0);
    }
    
    @Override
    public boolean hasMonitorings() {
        return (mMonitorings.size() > 0);
    }

    @Override
    public boolean hasOnCompleteAlerts() {
        return (mOnCompleteAlerts.size() > 0);
    }
    
    @Override
    public boolean hasOnCompleteLogs() {
        return (mOnCompleteLogs.size() > 0);
    }

    @Override
    public boolean hasOnCompleteMonitorings() {
        return (mOnCompleteMonitorings.size() > 0);
    }
    
    @Override
    public boolean hasOnStartAlerts() {
        return (mOnStartAlerts.size() > 0);
    }

    @Override
    public boolean hasOnStartLogs() {
        return (mOnStartLogs.size() > 0);
    }
    
    @Override
    public boolean hasOnStartMonitorings() {
        return (mOnStartMonitorings.size() > 0);
    }

    @Override
    public synchronized boolean removeAlert(Alert alert) {
        super.removeChild(alert);
        mOnStartAlerts.remove(alert);
        mOnCompleteAlerts.remove(alert);
        return mAlerts.remove(alert);
    }

    @Override
    public synchronized boolean removeLog(Log log) {
        super.removeChild(log);
        mOnStartLogs.remove(log);
        mOnCompleteLogs.remove(log);
        return mLogs.remove(log);
    }
    
    @Override
    public synchronized boolean removeMonitoring(Monitoring monitoring) {
        super.removeChild(monitoring);
        mOnStartMonitorings.remove(monitoring);
        mOnCompleteMonitorings.remove(monitoring);
        return mMonitorings.remove(monitoring);
    }
    
    @Override
    public Alert getAlert(int index) {
        return mAlerts.get(index);
    }

    @Override
    public Log getLog(int index) {
        return mLogs.get(index);
    }
    
    @Override
    public Monitoring getMonitoring(int index) {
        return mMonitorings.get(index);
    }
}
