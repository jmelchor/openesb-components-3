package com.sun.jbi.alerter;

/**
 * @author theyoz
 * @since 27/09/18
 */
public class AlerterSpi {
    public Alerter create() {
        return new AlerterImpl();
    }
}
