package com.sun.jbi.filebc.tests;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 17/01/18
 */
public class TestUtils {
    private static final File MODULE_ROOT;

    static {
        MODULE_ROOT = new File(TestUtils.class.getResource("/com/sun/jbi/filebc/input/BadInput.txt").getFile())
                .getParentFile()
                .getParentFile()
                .getParentFile()
                .getParentFile()
                .getParentFile()
                .getParentFile();
    }

    public static File getModuleRoot() {
        return MODULE_ROOT;
    }

    public static String xmlToString(Node node) throws TransformerException{
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(node), new StreamResult(writer));
        return writer.getBuffer().toString();
    }

    public static void nodeToText(Node node, StringBuffer sb) {
        sb.append(node.getTextContent());
        NodeList list = node.getChildNodes();

        if (list != null) {
            for (int i = 0; i < list.getLength(); i++) {
                Node childNode = list.item(i);

                nodeToText(childNode, sb);
            }
        }
    }

    public static File createTempDirectory() throws IOException {
        final File res = File.createTempFile(TestUtils.class.getName(), ".tmp");

        if (res.exists() && !res.delete()) {
            throw new IOException(
                    String.format("Couldn't create folder %s, temporary file did not delete", res.getAbsolutePath()));
        }

        if (!res.mkdirs()) {
            throw new IOException(String.format("Couldn't create folder %s", res.getAbsolutePath()));
        }

        return res;
    }

    public static File getTemporaryDirectoryRoot() {
        return new File(System.getProperty("java.io.tmpdir"));
    }
}
