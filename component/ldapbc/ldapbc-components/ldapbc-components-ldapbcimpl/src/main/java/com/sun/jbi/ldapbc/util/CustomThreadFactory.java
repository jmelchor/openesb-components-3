/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.jbi.ldapbc.util;

import java.util.concurrent.ThreadFactory;

/**
 *
 * @author Pymma
 */
public class CustomThreadFactory implements ThreadFactory{
    private int mCounter = 0  ;
    private final String mPrefix ; 
    
    public CustomThreadFactory (String prefix) {
        this.mPrefix = prefix ;         
    }

    @Override
    public Thread newThread(Runnable r) {
        return new Thread  (r,mPrefix + "-" + mCounter++ ); 
    }
    
}
