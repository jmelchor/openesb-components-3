/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.jbi.engine.bpel.core.bpel.trace;

import com.sun.bpel.model.meta.RActivity;
import com.sun.jbi.engine.bpel.core.bpel.engine.BPELProcessInstance;
import com.sun.jbi.engine.bpel.core.bpel.management.BPELSEManagement;
import com.sun.jbi.engine.bpel.core.bpel.model.runtime.Context;
import java.util.Properties;

/**
 *
 * @author polperez
 */
public interface BPELTraceManager {
    
    /**
     * The following methods are used manage the lifecycle of the BPELTrace manager
     * The original BPELTraceManager don't implement them. 
     * The methods are init, reset, shutdown. 
     * This methods are first invoked by the BPEL engine 
     * and propagated by the 
     * @param properties
     */
    
    // Init the trace manager 
    public void init (Properties properties);    
    // Reset trace manager properties
    public void reset (Properties properties);
    // shutdown trace manager 
    public void shutdown ();
    
    /**
     * The category under which BPEL processes will log
     */
    String BPEL_TRACE_CATEGORY = BPELTraceManagerImpl.class.getName(); //"sun-bpel-engine.Application";

    void alertBPInstanceChangeByAPI(String engineId, String bpName, String instanceId, String varName, BPELSEManagement.ActionType actionType);

    void alertBPInstanceTerminatedOnUnHandledFault(String engineId, String bpName, String instanceId, String cause);

    void alertDBConnectionRestored(String engineId);

    void alertEngineStatusChange(String engineId, int operationState);

    void alertSUStatusChange(String engineId, String suName, int operationState);

    void alertUnableToConnectToDB(String engineId, String cause);

    void alertUnableToCreateInstance(String engineId, String bpName, Throwable th);

    /**
     * Could be called for Virtual activities like CompensateUnit for which the
     * RActivity is null, hence care is taken to avoid trace calls.
     *
     * @param activity
     * @param context
     * @param process
     */
    void doTraceOnComplete(RActivity activity, Context context, BPELProcessInstance process);

    /**
     * Could be called for Virtual activities like CompensateUnit for which the
     * RActivity is null, hence care is taken to avoid trace calls.
     *
     * @param activity
     * @param context
     * @param process
     */
    void doTraceOnStart(RActivity activity, Context context, BPELProcessInstance process);
}