package com.sun.jbi.engine.bpel.core.bpel.xpath.functions.cache;

import com.sun.jbi.engine.bpel.core.bpel.cache.CacheManager;
import com.sun.jbi.engine.bpel.core.bpel.xpath.functions.BPWSFunctions;
import org.apache.commons.jxpath.ExpressionContext;
import org.apache.commons.jxpath.Pointer;
import org.apache.commons.jxpath.ri.EvalContext;
import org.w3c.dom.Node;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public class PutGetKeyCacheFunction extends AbstractCacheFunction {

    public PutGetKeyCacheFunction(CacheManager cacheManager) {
        super(cacheManager);
    }
        
    @Override
    public Object invoke(ExpressionContext context, Object[] parameters) {
        Node value = (Node) convertParam(parameters[1]);
        String key = (String)BPWSFunctions.convertParam(parameters[0]);
        return cacheManager.putGetKey(key, value);
    }
    
    private Object convertParam(Object src) {
        if (src instanceof Pointer) {
            return ((Pointer) src).getNode();
        }
        else if (src instanceof EvalContext) {
            return convertParam(((EvalContext) src).getCurrentNodePointer());
        }
        else if (src instanceof Node) { 
            //bug 615 input from doXSLTransform fuction is a Node
            return src;
        }
        return null;
    }
}
