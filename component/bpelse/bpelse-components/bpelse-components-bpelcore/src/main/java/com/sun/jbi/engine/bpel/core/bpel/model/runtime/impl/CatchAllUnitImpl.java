/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

 /*
 * @(#)CatchAllUnitImpl.java 
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.bpel.core.bpel.model.runtime.impl;

import java.util.Collection;
import java.util.Map;
import com.sun.bpel.model.BPELElement;
import com.sun.bpel.model.Catch;
import com.sun.bpel.model.CatchAll;
import com.sun.bpel.model.PartnerLink;
import com.sun.bpel.model.SingleActivityHolder;
import com.sun.bpel.model.meta.RActivity;
import com.sun.bpel.model.meta.RBPELProcess;
import com.sun.bpel.model.meta.RReply;
import com.sun.bpel.model.meta.RStartElement;
import com.sun.bpel.model.meta.RVariable;
import com.sun.jbi.engine.bpel.core.bpel.engine.BPELProcessInstance;
import com.sun.jbi.engine.bpel.core.bpel.engine.BusinessProcessInstanceThread;
import com.sun.jbi.engine.bpel.core.bpel.engine.ICallFrame;
import com.sun.jbi.engine.bpel.core.bpel.engine.MessageContainer;
import com.sun.jbi.engine.bpel.core.bpel.exception.BPELRuntimeException;
import com.sun.jbi.engine.bpel.core.bpel.model.runtime.ActivityContainerUnit;
import com.sun.jbi.engine.bpel.core.bpel.model.runtime.ActivityUnit;
import com.sun.jbi.engine.bpel.core.bpel.model.runtime.ActivityUnitFactory;
import com.sun.jbi.engine.bpel.core.bpel.model.runtime.Context;
import com.sun.jbi.engine.bpel.core.bpel.model.runtime.Fault;
import com.sun.jbi.engine.bpel.core.bpel.model.runtime.FaultHandlingContext;
import com.sun.jbi.engine.bpel.core.bpel.model.runtime.RequiredObjects;
import com.sun.jbi.engine.bpel.core.bpel.model.runtime.RuntimeEventHandlers;
import com.sun.jbi.engine.bpel.core.bpel.model.runtime.RuntimePartnerLink;
import com.sun.jbi.engine.bpel.core.bpel.model.runtime.RuntimeVariable;
import com.sun.jbi.engine.bpel.core.bpel.model.runtime.StateContext;
import com.sun.jbi.engine.bpel.core.bpel.model.runtime.Unit;
import com.sun.jbi.engine.bpel.core.bpel.model.runtime.WSMessage;
import java.util.logging.Logger;

/**
 * CatchAll activity unit implementation
 *
 * @author Sun Microsystems
 * @author Pymma OpenESB Team 
 * 11/2020: Add trace on the log file when a catchAll occurs
 */
public class CatchAllUnitImpl implements ActivityContainerUnit, Context {

    // the runtimevariable is moved to CatchAllUnitImpl as we need this for 
    // dereferencing the variable from the ws message, once this activity completes
    protected RuntimeVariable mRuntimeVariable;
    protected BPELElement mStaticModel;
    protected boolean mIsProcessLevelFaultHandling;
    protected Context mContext;
    protected SingleActivityHolder mActHolder;
    protected Fault mFault;
    private static final Logger LOGGER = Logger.getLogger(CatchAllUnitImpl.class.getName());

    /**
     * Default constructor
     */
    public CatchAllUnitImpl() {
        //Empty
    }

    /**
     * Creates a new CatchAllUnitImpl object.
     *
     * @param context
     * @param catchAll catchall
     * @param fault TODO
     */
    public CatchAllUnitImpl(Context context, CatchAll catchAll, Fault fault) {
        this(context, (SingleActivityHolder) catchAll, fault);
        mStaticModel = catchAll;
        this.traceCatchAll(context, catchAll,fault); 
    }

    /**
     * Constructor invoked by catch implementation 
     * @param context
     * @param singleActHolder
     * @param fault 
     */
    protected CatchAllUnitImpl(Context context, SingleActivityHolder singleActHolder, Fault fault) {
        mContext = context;
        mActHolder = singleActHolder;
        mFault = fault;
        mIsProcessLevelFaultHandling = (mContext.getParentContext() == null);
    }

    /**
     * @throws java.lang.Exception
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.ActivityUnit#doAction(ICallFrame,
     * BusinessProcessInstanceThread, RequiredObjects)
     */
    @Override
    public boolean doAction(
            ICallFrame frame, BusinessProcessInstanceThread bpit, RequiredObjects rObjs ) throws Exception {

      long branchId = 0L;

        if (mIsProcessLevelFaultHandling) {
            branchId = RBPELProcess.DEFAULT_PROCESS_BRANCH_ID;
        } else {
            branchId = ((ActivityUnit) mContext).getBranchId();
        }

        ActivityUnit childActUnit = ActivityUnitFactory.getInstance().createActivityUnit(
                this, (Unit) this, (RActivity) mActHolder.getActivity(), branchId);

        if (CodeReUseHelper.executeChildActivities(frame, bpit, rObjs, childActUnit)) {

            if (mStaticModel instanceof Catch) {
                if (mRuntimeVariable != null) {
                    WSMessage message = mRuntimeVariable.getWSMessage();
                    if (message != null) {
                        message.removeInternalReference(mRuntimeVariable.getVariableDef());
                    }
                }
            }
            return true;

        } else {
            return false;
        }
    }

    @Override
    public boolean doResumeAction(ActivityUnit childActUnit, ICallFrame frame,
            BusinessProcessInstanceThread bpit, RequiredObjects rObjs) throws Exception {
        return ((StructuredActivityUnitImpl) mContext).doResumeAction(null, frame, bpit, rObjs);
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.Unit#getEnclosingUnit()
     */
    @Override
    public Unit getEnclosingUnit() {
        return (ActivityUnit) mContext;
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.Unit#getEnclosingScopeUnit()
     */
    @Override
    public ActivityUnit getEnclosingScopeUnit() {
        return (ActivityUnit) mContext;
    }

    /**
     * @see com.sun.jbi.engine.bpel.core.bpel.model.runtime.Unit#getContext()
     */
    @Override
    public Context getContext() {
        return mContext;
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.ActivityContainerUnit#getStaticModel()
     */
    @Override
    public BPELElement getStaticModel() {
        return mStaticModel;
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.Context#getProcessInstance()
     */
    @Override
    public BPELProcessInstance getProcessInstance() {
        return mContext.getProcessInstance();
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.Context#initUponRecovery(Collection,
     * Collection)
     */
    @Override
    public void initUponRecovery(Collection<RuntimeVariable> runtimeVariables, Collection<RuntimePartnerLink> runtimePLinks) {
        // TODO: ? 
        throw new UnsupportedOperationException();
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.Context#initEHUponRecovery(com.sun.jbi.engine.bpel.core.bpel.model.runtime.RuntimeEventHandlers)
     */
    @Override
    public void initEHUponRecovery(RuntimeEventHandlers rEH) {
        throw new UnsupportedOperationException();
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.IMAScope#addRequest(com.sun.bpel.model.meta.RStartElement,
     * com.sun.jbi.engine.bpel.core.bpel.engine.MessageContainer)
     */
    @Override
    public void addRequest(RStartElement rcv, MessageContainer req) throws BPELRuntimeException {
        mContext.addRequest(rcv, req);
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.IMAScope#removeRequest(com.sun.bpel.model.meta.RStartElement,
     * com.sun.jbi.engine.bpel.core.bpel.engine.MessageContainer)
     */
    @Override
    public void removeRequest(RStartElement rcv, MessageContainer req) throws BPELRuntimeException {
        mContext.removeRequest(rcv, req);
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.IMAScope#addToCRMPUpdateList(String
     * updateValueKey)
     */
    public void addToCRMPUpdateList(String updateValueKey) {
        mContext.addToCRMPUpdateList(updateValueKey);
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.IMAScope#crmpUpdateListContains(String
     * updateValueKey)
     */
    @Override
    public boolean crmpUpdateListContains(String updateValueKey) {
        return mContext.crmpUpdateListContains(updateValueKey);
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.IMAScope#removeRequest(com.sun.bpel.model.meta.RReply)
     */
    @Override
    public MessageContainer removeRequest(RReply reply) {
        return mContext.removeRequest(reply);
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.IMAScope#sendErrorsForPendingRequests(Exception
     * error)
     */
    @Override
    public void sendErrorsForPendingRequests(Exception error) {
        throw new UnsupportedOperationException();
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.IMAScope#completePendingInOnlyRequests()
     */
    @Override
    public void completePendingInOnlyRequests() {
        throw new UnsupportedOperationException();
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.VariableScope#getRuntimeVariable(com.sun.bpel.model.meta.RVariable)
     */
    @Override
    public RuntimeVariable getRuntimeVariable(RVariable variable) {
        return mContext.getRuntimeVariable(variable);
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.VariableScope#getRuntimeVariables()
     */
    @Override
    public Map getRuntimeVariables() {
        return mContext.getRuntimeVariables();
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.VariableScope#setRuntimeVariable(com.sun.jbi.engine.bpel.core.bpel.model.runtime.RuntimeVariable)
     */
    @Override
    public void setRuntimeVariable(RuntimeVariable runtimeVariable) {
        mContext.setRuntimeVariable(runtimeVariable);
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.Context#getParentContext()
     */
    @Override
    public Context getParentContext() {
        return mContext.getParentContext();
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.IMAScope#declareDefaultMessageExchange()
     */
    @Override
    public void declareDefaultMessageExchange() {
        throw new UnsupportedOperationException();
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.PartnerLinkScope#getRuntimePartnerLink(com.sun.bpel.model.PartnerLink)
     */
    @Override
    public RuntimePartnerLink getRuntimePartnerLink(PartnerLink pLink) {
        return mContext.getRuntimePartnerLink(pLink);
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.PartnerLinkScope#setRuntimePartnerLink(com.sun.bpel.model.PartnerLink,
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.RuntimePartnerLink)
     */
    @Override
    public void setRuntimePartnerLink(PartnerLink pLink, RuntimePartnerLink runtimePLink) {
        mContext.setRuntimePartnerLink(pLink, runtimePLink);
    }

    @Override
    public Map getRuntimePartnerLinks() {
        return mContext.getRuntimePartnerLinks();
    }

    @Override
    public FaultHandlingContext getFaultHandlingContext() {
        return mContext.getFaultHandlingContext();
    }

    /**
     * @return 
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.Context#getStateContext()
     */
    @Override
    public StateContext getStateContext() {
        return mContext.getStateContext();
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.VariableScope#createRuntimeVariable(com.sun.bpel.model.meta.RVariable)
     */
    @Override
    public RuntimeVariable createRuntimeVariable(RVariable variable) {
        return mContext.createRuntimeVariable(variable);
    }

    /**
     * @see
     * com.sun.jbi.engine.bpel.core.bpel.model.runtime.PartnerLinkScope#createRuntimePartnerLink(PartnerLink)
     */
    @Override
    public RuntimePartnerLink createRuntimePartnerLink(PartnerLink partnerlink) {
        return mContext.createRuntimePartnerLink(partnerlink);
    }
    
    private void traceCatchAll(Context context, CatchAll catchAll, Fault fault){ 
         /**
         * Add a message to understand the fault that triggered the CatchAll
         * There are two cases. The first occurs when a fault is sent and catch
         * by a CatchAll activity The second occurs when an exception is
         * returned and catch by a CatchAll activity
         *
         * In the first case the exception is null
         */
        StringBuilder messageFault = new StringBuilder();
        String serviceUnitName = context.getProcessInstance().getBPELProcessManager().getServiceUnitName();         
        messageFault.append("The service Unit ").append(serviceUnitName).append(" triggered a CatchAll activity and catch the following fault: ");
        if (mFault.getException() == null) {
            messageFault.append(this.mFault.getName().toString());
            LOGGER.warning(messageFault.toString());
        } else {
            if (mFault.getException().getCause() != null) {
                messageFault.append(mFault.getException().getCause().getMessage());
                LOGGER.warning(messageFault.toString());
            } else {
                if (mFault.getException().getMessage() != null) {
                    messageFault.append(mFault.getException().getMessage());
                    LOGGER.warning(messageFault.toString());
                } else { 
                    messageFault.append(" Unable to define the fault or exception that triggered the CatchAll activity"); 
                }                    
            }
        }

    }
}
