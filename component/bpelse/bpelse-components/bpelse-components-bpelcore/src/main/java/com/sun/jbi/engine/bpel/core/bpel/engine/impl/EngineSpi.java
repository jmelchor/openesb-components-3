package com.sun.jbi.engine.bpel.core.bpel.engine.impl;

import com.sun.jbi.engine.bpel.core.bpel.engine.Engine;

import javax.naming.InitialContext;
import java.util.Properties;

/**
 * @author theyoz
 * @since 27/09/18
 */
public class EngineSpi {
    public Engine create(Properties properties, InitialContext initialContext) {
        return new EngineImpl(properties, initialContext);
    }
}
