package com.sun.jbi.engine.bpel.core.bpel.xpath.functions.cache;

import com.sun.jbi.engine.bpel.core.bpel.cache.CacheManager;
import com.sun.jbi.engine.bpel.core.bpel.xpath.functions.BPWSFunctions;
import org.apache.commons.jxpath.ExpressionContext;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public class ContainsCacheFunction extends AbstractCacheFunction {

    public ContainsCacheFunction(CacheManager cacheManager) {
        super(cacheManager);
    }
    
    @Override
    public Object invoke(ExpressionContext context, Object[] parameters) {
        boolean contains = cacheManager.contains((String) BPWSFunctions.convertParam(parameters[0]));
        return contains;
    }

}
