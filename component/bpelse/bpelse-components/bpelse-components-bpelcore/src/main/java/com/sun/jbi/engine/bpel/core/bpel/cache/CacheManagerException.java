/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.jbi.engine.bpel.core.bpel.cache;

/**
 *
 * @author polperez
 */
public class CacheManagerException extends Exception {

    /**
     * Creates a new instance of <code>CacheManagerException</code> without
     * detail message.
     */
    public CacheManagerException() {
    }

    /**
     * Constructs an instance of <code>CacheManagerException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public CacheManagerException(String msg) {
        super(msg);
    }
}
