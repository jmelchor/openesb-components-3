package com.sun.jbi.engine.bpel.core.bpel.xpath.functions.cache;

import com.sun.jbi.engine.bpel.core.bpel.cache.CacheManager;
import com.sun.jbi.engine.bpel.core.bpel.xpath.functions.BPWSFunctions;
import org.apache.commons.jxpath.ExpressionContext;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public class GetCacheFunction extends AbstractCacheFunction {
    
    public GetCacheFunction(CacheManager cacheManager) {
        super(cacheManager);
    }
    
    public Object invoke(ExpressionContext context, Object[] parameters) {
        return cacheManager.get((String) BPWSFunctions.convertParam(parameters[0]));
    }

}
