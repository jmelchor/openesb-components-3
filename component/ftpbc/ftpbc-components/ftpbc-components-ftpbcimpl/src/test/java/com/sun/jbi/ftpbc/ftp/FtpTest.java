/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FtpTest.java 
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.ftpbc.ftp;

import com.sun.jbi.ftpbc.ftp.exception.ConfigurationException;
import com.sun.jbi.ftpbc.ftp.exception.FtpFileException;
import org.testng.annotations.Test;

/**
 * JUnit based test.
 *
 * 
 * 
 * 
 * @author Harry Liu (harry.liu@sun.com)
 * @author theyoz
 *
 * Copyright 2006 Sun Microsystems, Inc. All Rights Reserved.
 */
public class FtpTest extends AbstractFtpTest {
    /**
     * Test of initialize method, of class com.sun.jbi.ftpbc.ftp.FtpInterface.
     */
    @Test(expectedExceptions = ConfigurationException.class)
    public void testInitialize() throws Exception {
        System.out.println("initialize");
        
        ftp = new FtpInterface();
        ftp.initialize(null);
    }

    /*
     * Test of getConfiguration method, of class com.sun.jbi.ftpbc.ftp.FtpInterface.
     */
    @Test
    public void testGetConfiguration() throws Exception {
        System.out.println("getConfiguration");
        
        assert ftp.getConfiguration() != null;
        
    }
    
    /**
     * Test of setConfiguration method, of class com.sun.jbi.ftpbc.ftp.FtpInterface.
     */
    @Test
    public void testSetConfiguration() throws Exception {
        System.out.println("setConfiguration");
        
        ftp.setConfiguration(null);
        assert ftp.getConfiguration() == null;

        FtpFileConfiguration cfg = new FtpFileConfiguration(null);
        
        ftp.setConfiguration(cfg);
        assert cfg.equals(ftp.getConfiguration());
    }
    
    /**
     * Test of getClient method, of class com.sun.jbi.ftpbc.ftp.FtpInterface.
     */
    @Test
    public void testGetClient() throws Exception {
        System.out.println("getClient");
        
        assert ftp.getClient() != null;
    }
    
    /**
     * Test of getProvider method, of class com.sun.jbi.ftpbc.ftp.FtpInterface.
     */
    @Test
    public void testGetProvider() throws Exception {
        System.out.println("getProvider");
        
        assert ftp.getProvider() != null;
    }
    
    /**
     * Test of setClient method, of class com.sun.jbi.ftpbc.ftp.FtpInterface.
     */
    @Test
    public void testSetClient() throws Exception {
        System.out.println("setClient");
        
        ftp.setClient(null);
        assert ftp.getClient() == null;

        FtpFileClient client = new FtpFileClientImpl();
        client.initialize(ftp);
        
        ftp.setClient(client);
        assert client.equals(ftp.getClient());
        
    }
    
    /**
     * Test of setProvider method, of class com.sun.jbi.ftpbc.ftp.FtpInterface.
     */
    @Test
    public void testSetProvider() throws Exception {
        System.out.println("setProvider");
        
        ftp.setProvider(null);
        assert ftp.getProvider() == null;

        FtpFileProvider provider = new FtpFileProviderImpl();
        provider.setDirListingStyle("UNIX");
        
        ftp.setProvider(provider);
        assert provider.equals(ftp.getProvider());
        
    }
    
    /**
     * Test of getStateManager method, of class com.sun.jbi.ftpbc.ftp.FtpInterface.
     */
    @Test
    public void testGetStateManager() throws Exception {
        System.out.println("getStateManager");
        
        assert ftp.getStateManager() != null;
    }
    
    /**
     * Test of getState method, of class com.sun.jbi.ftpbc.ftp.FtpInterface.
     */
    @Test
    public void testGetState() throws Exception {
        System.out.println("getState");
        
        assert ftp.getState() != null;
    }
    
    /**
     * Test of isStateChanged method, of class com.sun.jbi.ftpbc.ftp.FtpInterface.
     */
    @Test
    public void testIsStateChanged() throws Exception {
        System.out.println("isStateChanged");
        
        assert !ftp.isStateChanged();
    }
    
    /**
     * Test of setStateChanged method, of class com.sun.jbi.ftpbc.ftp.FtpInterface.
     */
    @Test
    public void testSetStateChanged() throws Exception {
        System.out.println("setStateChanged");
        
        ftp.setStateChanged(true);
        assert ftp.isStateChanged();
    }
}
