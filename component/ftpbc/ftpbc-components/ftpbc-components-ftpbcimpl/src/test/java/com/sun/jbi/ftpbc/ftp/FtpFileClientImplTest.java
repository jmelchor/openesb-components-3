/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FtpFileClientImplTest.java 
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ftpbc.ftp;

import com.sun.jbi.ftpbc.ftp.exception.FtpFileException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Properties;

/**
 * TestNG based test.
 *
 * @author Harry Liu (harry.liu@sun.com)
 * @author theyoz
 *
 * Copyright 2006 Sun Microsystems, Inc. All Rights Reserved.
 */
public class FtpFileClientImplTest extends AbstractFtpTest {
    private FtpFileClientImpl defaultInstance;

    @Override
    protected void afterInitializeFtp() throws Exception {
        super.afterInitializeFtp();

        defaultInstance = (FtpFileClientImpl) ftp.getClient();

        if (!defaultInstance.isOpen()) {
            defaultInstance.open();
        }
    }


    /**
     * Test wrong null value on initialize
     */
    @Test(expectedExceptions = {FtpFileException.class})
    public void testInitializeFail() throws Exception {
        FtpFileClientImpl instance = new FtpFileClientImpl();
        instance.initialize(null);
    }


    /**
     * Test Open and Close
     */
    @Test
    public void testOpenAndClose() throws Exception {
        FtpFileClientImpl instance = new FtpFileClientImpl();
        FtpInterface ftp = new FtpInterface();
        ftp.initialize(this.initializationProperties);

        instance.initialize(ftp);
        assert instance.isOpen() : "Instance should be opened after initialization";
        instance.close();
        assert !instance.isOpen() : "Instance should be closed after calling close()";
        instance.open();
        assert instance.isOpen() : "Instance should be opened after calling open()";
        instance.close();
        assert !instance.isOpen() : "Instance should be closed (again) after initialization";
    }

    @DataProvider
    public Object[][] dpOpen_encoding() {
        return new Object[][] {
                {FtpFileConfigConstants.DEFAULT_CNTRL_ENCODING},
                {FtpFileConfigConstants.ENCODING_EUC_JP},
                {FtpFileConfigConstants.ENCODING_JIS},
                {FtpFileConfigConstants.ENCODING_SJIS}
        };
    }

    /**
     * Test of connect method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test(dataProvider = "dpOpen_encoding")
    public void testOpen_encoding(String encoding) throws Exception {
        System.out.println("open_encoding");
        FtpFileClientImpl instance = new FtpFileClientImpl();
        FtpInterface ftp = new FtpInterface();
        ftp.initialize(this.initializationProperties);

        instance.initialize(ftp);
        instance.open(encoding);
        instance.close();
    }

    /**
     * Test of initialConfigValues method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test(expectedExceptions = FtpFileException.class)
    public void testInitialConfigValues() throws Exception {
        System.out.println("initialConfigValues");
        FtpFileClientImpl instance = new FtpFileClientImpl();
        FtpInterface ftp = new FtpInterface();
        ftp.initialize(this.initializationProperties);
        instance.initialize(ftp);
        instance.initialConfigValues(new Properties());
        instance.open();
    }

    @Test(expectedExceptions = FtpFileException.class)
    public void testInitialConfigValuesFails() throws Exception {
        System.out.println("initialConfigValues");
        FtpFileClientImpl instance = new FtpFileClientImpl();
        FtpInterface ftp = new FtpInterface();
        ftp.initialize(this.initializationProperties);
        instance.initialize(ftp);
        instance.initialConfigValues(null);
    }

    /**
     * Test of doRawCommands method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testDoRawCommands() throws Exception {
        System.out.println("doRawCommands");

        String commands = "CWD /;PWD";
        defaultInstance.doRawCommands(commands);
    }

    /**
     * Test of doRawCommands method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test(expectedExceptions = {FtpFileException.class})
    public void testDoRawCommandsFails() throws Exception {
        System.out.println("doRawCommands");

        String commands = "BAD command";
        defaultInstance.doRawCommands(commands);
    }

    /*
     * Test of getPayload method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testGetPayload() {
        System.out.println("getPayload");

        byte[] result = defaultInstance.getPayload();
        assert result == null;
    }

    /**
     * Test of setPayload method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testSetPayload() {
        System.out.println("setPayload");

        byte[] newPayload = "data".getBytes();

        defaultInstance.setPayload(newPayload);
        assert Arrays.equals(newPayload, defaultInstance.getPayload());

    }

    /**
     * Test of restoreConfigValues method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testRestoreConfigValues() throws Exception {
        System.out.println("restoreConfigValues");

        defaultInstance.restoreConfigValues();
    }

    /**
     * Test of connect method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testConnect() throws Exception {
        System.out.println("connect");

        defaultInstance.connect();

    }

    /**
     * Test of connect method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testConnect_encoding() throws Exception {
        System.out.println("connect_encoding");

        defaultInstance.connect(FtpFileConfigConstants.DEFAULT_CNTRL_ENCODING);
        defaultInstance.connect(FtpFileConfigConstants.ENCODING_EUC_JP);
        defaultInstance.connect(FtpFileConfigConstants.ENCODING_JIS);
        defaultInstance.connect(FtpFileConfigConstants.ENCODING_SJIS);
    }

    /**
     * Test of disconnect method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testDisconnect() {
        System.out.println("disconnect");

        defaultInstance.disconnect();
    }

    /**
     * Test of isConnected method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testIsConnected() {
        System.out.println("isConnected");

        assert defaultInstance.isConnected();
    }

    /**
     * Test of setInputStreamAdapter method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testSetInputStreamAdapter() {
        System.out.println("setInputStreamAdapter");

        defaultInstance.setPayload("Not-Null".getBytes());

        defaultInstance.setInputStreamAdapter(null);

        assert defaultInstance.getPayload() == null : "setInputStreamAdapter() will reset payload to null";

    }

    /**
     * Test of setOutputStreamAdapter method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testSetOutputStreamAdapter() {
        System.out.println("setOutputStreamAdapter");

        defaultInstance.setPayload("Not-Null".getBytes());

        defaultInstance.setOutputStreamAdapter(null);

        assert defaultInstance.getPayload() == null: "setOutputStreamAdapter() will reset payload to null";
    }

    /**
     * Test of getResolvedNamesForGet method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testGetResolvedNamesForGet() throws Exception {
        Object o = defaultInstance.getResolvedNamesForGet();
        assert o != null;
    }

    /**
     * Test of getResolvedNamesForPut method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testGetResolvedNamesForPut() throws Exception {
        Object o = defaultInstance.getResolvedNamesForPut();
        assert o != null;
    }

    /**
     * Test of doPostTransferGet method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testDoPostTransferGet() throws Exception {
        System.out.println("doPostTransferGet");
        defaultInstance.initialize(ftp);
        defaultInstance.connect();
        TransferNamesAndCommands tncg = defaultInstance.getResolvedNamesForGet();
        defaultInstance.doPostTransferGet(tncg);
    }

    /**
     * Test of doPreTransferGet method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testDoPreTransferGet() throws Exception {
        System.out.println("doPreTransferGet");
        TransferNamesAndCommands tncg = defaultInstance.getResolvedNamesForGet();
        defaultInstance.doPreTransferGet(tncg);
    }

    /**
     * Test of doTransferGet method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testDoTransferGet() throws Exception {
        System.out.println("doTransferGet");
        // originally the test assume the target location is blank
        // but that is not true - certain configuration change on the target server
        // can result in that :
        // after 
        // TransferNamesAndCommands tncg = defaultInstance.getResolvedNamesForGet()
        // tncg.getTargetDirectoryName() can be a non-empty value
        // and/or
        // tncg.getTargetFileName() can be a non-empty value
        // so the test should be changed to accommodate these;
        TransferNamesAndCommands tncg = defaultInstance.getResolvedNamesForGet();
        if ((tncg.getTargetDirectoryName() != null && tncg.getTargetDirectoryName().trim().length() > 0) || (tncg.getTargetFileName() != null && tncg.getTargetFileName().trim().length() > 0)) {
            // do not further try get
            return;
        }
        defaultInstance.doTransferGet(tncg);
    }

    /**
     * Test of doPostTransferPut method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testDoPostTransferPut() throws Exception {
        System.out.println("doPostTransferPut");
        TransferNamesAndCommands tncp = defaultInstance.getResolvedNamesForPut();
        defaultInstance.doPostTransferPut(tncp);
    }

    /**
     * Test of doPreTransferPut method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testDoPreTransferPut() throws Exception {
        System.out.println("doPreTransferPut");
        TransferNamesAndCommands tncp = defaultInstance.getResolvedNamesForPut();
        defaultInstance.doPreTransferPut(tncp);
    }

    /**
     * Test of doTransferPut method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testDoTransferPut() throws Exception {
        System.out.println("doTransferPut");
        defaultInstance.setPayload("any data".getBytes());
        TransferNamesAndCommands tncp = defaultInstance.getResolvedNamesForPut();
        tncp.setTargetDirectoryName("temp");
        defaultInstance.doTransferPut(tncp);
    }

    /**
     * Test of put method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test(expectedExceptions = FtpFileException.class)
    public void testPut() throws FtpFileException {
        System.out.println("put");
        defaultInstance.setPayload("any data".getBytes());
        defaultInstance.put();
    }

    /**
     * Test of undoPreTransferGet method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testUndoPreTransferGet() throws Exception {
        System.out.println("undoPreTransferGet");

        TransferNamesAndCommands tncg = defaultInstance.getResolvedNamesForGet();
        defaultInstance.undoPreTransferGet(tncg);
    }

    /**
     * Test of undoTransferGet method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testUndoTransferGet() throws Exception {
        System.out.println("undoTransferGet");

        TransferNamesAndCommands tncg = defaultInstance.getResolvedNamesForGet();
        defaultInstance.undoTransferGet(tncg);
    }

    /**
     * Test of undoPreTransferPut method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testUndoPreTransferPut() throws Exception {
        System.out.println("undoPreTransferPut");

        TransferNamesAndCommands tncp = defaultInstance.getResolvedNamesForPut();
        defaultInstance.undoPreTransferPut(tncp);
    }

    /**
     * Test of undoTransferPut method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testUndoTransferPut() throws Exception {
        System.out.println("undoTransferPut");
        TransferNamesAndCommands tncp = defaultInstance.getResolvedNamesForPut();
        defaultInstance.undoTransferPut(tncp); }

    /**
     * Test of undoPostTransferGet method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testUndoPostTransferGet() throws Exception {
        System.out.println("undoPostTransferGet");
        TransferNamesAndCommands tncg = defaultInstance.getResolvedNamesForGet();
        defaultInstance.undoPostTransferGet(tncg);
    }

    /**
     * Test of undoPostTransferPut method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testUndoPostTransferPut() throws Exception {
        TransferNamesAndCommands tncp = defaultInstance.getResolvedNamesForPut();
        defaultInstance.undoPostTransferPut(tncp);
    }

    /**
     * Test of cleanupPostTransferGet method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testCleanupPostTransferGet() throws Exception {
        System.out.println("cleanupPostTransferGet");
        TransferNamesAndCommands tncg = defaultInstance.getResolvedNamesForGet();
        defaultInstance.cleanupPostTransferGet(tncg);
    }

    /**
     * Test of cleanupPostTransferPut method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testCleanupPostTransferPut() throws Exception {
        System.out.println("cleanupPostTransferPut");
        TransferNamesAndCommands tncp = defaultInstance.getResolvedNamesForPut();
        defaultInstance.cleanupPostTransferPut(tncp);
    }

    /**
     * Test of cleanupPreTransferGet method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testCleanupPreTransferGet() throws Exception {
        System.out.println("cleanupPreTransferGet");
        TransferNamesAndCommands tncg = defaultInstance.getResolvedNamesForGet();
        defaultInstance.cleanupPreTransferGet(tncg);
    }

    /**
     * Test of cleanupPreTransferPut method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testCleanupPreTransferPut() throws Exception {
        System.out.println("cleanupPreTransferPut");
        TransferNamesAndCommands tncp = defaultInstance.getResolvedNamesForPut();
        defaultInstance.cleanupPreTransferPut(tncp);
    }

    /**
     * Test of cleanupRawCommands method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testCleanupRawCommands() throws Exception {
        System.out.println("cleanupRawCommands");
        defaultInstance.cleanupRawCommands(null);
    }

    /**
     * Test of cleanupTransferGet method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testCleanupTransferGet() throws Exception {
        System.out.println("cleanupTransferGet");
        TransferNamesAndCommands tncg = defaultInstance.getResolvedNamesForGet();
        defaultInstance.cleanupTransferGet(tncg);
    }

    /**
     * Test of cleanupTransferPut method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testCleanupTransferPut() throws Exception {
        System.out.println("cleanupTransferPut");
        TransferNamesAndCommands tncp = defaultInstance.getResolvedNamesForPut();
        defaultInstance.cleanupTransferPut(tncp);
    }

    /**
     * Test of undoRawCommands method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testUndoRawCommands() throws Exception {
        System.out.println("undoRawCommands");
        defaultInstance.undoRawCommands(null);
    }

    /**
     * Test of get method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test(expectedExceptions = FtpFileException.class)
    public void testGet() throws Exception {
        System.out.println("get");
        TransferNamesAndCommands rn4get = defaultInstance.getResolvedNamesForGet();
        if ((rn4get.getTargetDirectoryName() != null && rn4get.getTargetDirectoryName().trim().length() > 0) || (rn4get.getTargetFileName() != null && rn4get.getTargetFileName().trim().length() > 0)) {
            // if the server is configured such that
            // the target dir and/or file is not empty
            // the following assert might not be holding
            // so bail out here - instead of failing the test
            return;
        }
        defaultInstance.get();
    }

    /**
     * Test of getIfExists method, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testGetIfExists() throws Exception {
        System.out.println("getIfExists");
        defaultInstance.getIfExists();
    }

    /**
     * Test of constructor, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test
    public void testFtpFileClientImpl() throws Exception {
        System.out.println("FtpFileClientImpl");

        new FtpFileClientImpl();
        new FtpFileClientImpl(ftp);
        new FtpFileClientImpl(new FtpInterface());
    }

    /**
     * Test of constructor, of class com.sun.jbi.ftpbc.ftp.FtpFileClientImpl.
     */
    @Test(expectedExceptions = FtpFileException.class)
    public void testFtpFileClientImplFails() throws Exception {
        System.out.println("FtpFileClientImpl");

        new FtpFileClientImpl(null);
    }
}
