/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FtpFileTransferNamesAndCommandsPutTest.java 
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.ftpbc.ftp;

import org.testng.annotations.Test;

/**
 * JUnit based test.
 *
 * 
 * 
 * 
 * @author Harry Liu (harry.liu@sun.com)
 * @author theyoz
 *
 * Copyright 2006 Sun Microsystems, Inc. All Rights Reserved.
 */
public class FtpFileTransferNamesAndCommandsPutTest extends AbstractFtpTest {
    FtpFileTransferNamesAndCommandsPut instance;

    @Override
    protected void afterInitializeFtp() throws Exception {
        super.afterInitializeFtp();

        instance = new FtpFileTransferNamesAndCommandsPut(ftp);
    }

    /**
     * Test of resolveTargetLocation method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommandsPut.
     */
    @Test
    public void testResolveTargetLocation() throws Exception {
        System.out.println("resolveTargetLocation");
        
        instance.resolveTargetLocation();
        
    }

    /**
     * Test of resolveStageLocation method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommandsPut.
     */
    @Test
    public void testResolveStageLocation() throws Exception {
        System.out.println("resolveStageLocation");
        
        instance.resolveStageLocation();
        
    }

    /**
     * Test of getStageDirectoryName method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommandsPut.
     */
    @Test
    public void testGetStageDirectoryName() throws Exception {
        System.out.println("getStageDirectoryName");
        
        assert instance.getStageDirectoryName() == null;
    }

    /**
     * Test of getStageFileName method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommandsPut.
     */
    @Test
    public void testGetStageFileName() throws Exception {
        System.out.println("getStageFileName");
        
        assert instance.getStageFileName() == null;
    }

    /**
     * Test of constructor, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommandsPut.
     */
    @Test
    public void testFtpFileTransferNamesAndCommandsPut() throws Exception {
        System.out.println("FtpFileTransferNamesAndCommandsPut");
        
        new FtpFileTransferNamesAndCommandsPut(ftp);
        new FtpFileTransferNamesAndCommandsPut(new FtpFileTransferNamesAndCommandsPut(ftp), new FtpFileConfiguration(ftp));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testFtpFileTransferNamesAndCommandsPutFails1() {
        new FtpFileTransferNamesAndCommandsPut(null);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testFtpFileTransferNamesAndCommandsPutFails2() {
        new FtpFileTransferNamesAndCommandsPut(null, null);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testFtpFileTransferNamesAndCommandsPutFails3() throws Exception {
        new FtpFileTransferNamesAndCommandsPut(null, new FtpFileConfiguration(ftp));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testFtpFileTransferNamesAndCommandsPutFails4() {
        new FtpFileTransferNamesAndCommandsPut(new FtpFileTransferNamesAndCommandsPut(ftp), null);
    }
}
