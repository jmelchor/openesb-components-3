/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FtpFileTransferNamesAndCommandsTest.java 
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.ftpbc.ftp;

import org.testng.annotations.Test;

/**
 * JUnit based test.
 *
 * 
 * 
 * 
 * @author Harry Liu (harry.liu@sun.com)
 * @author theyoz
 *
 * Copyright 2006 Sun Microsystems, Inc. All Rights Reserved.
 */
public class FtpFileTransferNamesAndCommandsTest extends AbstractFtpTest {
    FtpFileTransferNamesAndCommands instance;

    @Override
    protected void afterInitializeFtp() throws Exception {
        super.afterInitializeFtp();

        instance = new FtpFileTransferNamesAndCommandsGet(ftp);
    }

    /**
     * Test of getTargetDirectoryName method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommands.
     */
    @Test
    public void testGetTargetDirectoryName() throws Exception {
        System.out.println("getTargetDirectoryName");
        // server can be configured such that 
        // the target directory name is not an empty value
        // String expResult = "";
        assert instance.getTargetDirectoryName() != null;
    }
    
    /**
     * Test of getTargetFileName method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommands.
     */
    @Test
    public void testGetTargetFileName() throws Exception {
        System.out.println("getTargetFileName");
        // server can be configured such that 
        // the target directory name is not an empty value
        //String expResult = "";
        assert instance.getTargetFileName() != null;
    }
    
    /**
     * Test of getPreTransferCommand method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommands.
     */
    @Test
    public void testGetPreTransferCommand() throws Exception {
        System.out.println("getPreTransferCommand");
        
        assert "None".equals(instance.getPreTransferCommand());
    }
    
    /**
     * Test of getPreDirectoryName method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommands.
     */
    @Test
    public void testGetPreDirectoryName() throws Exception {
        System.out.println("getPreDirectoryName");
        
        assert "".equals(instance.getPreDirectoryName());
    }
    
    /**
     * Test of getPreFileName method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommands.
     */
    @Test
    public void testGetPreFileName() throws Exception {
        System.out.println("getPreFileName");
        
        assert "".equals(instance.getPreFileName());
    }
    
    /**
     * Test of getPostTransferCommand method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommands.
     */
    @Test
    public void testGetPostTransferCommand() throws Exception {
        System.out.println("getPostTransferCommand");
        
        assert "None".equals(instance.getPostTransferCommand());
    }
    
    /**
     * Test of getPostDirectoryName method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommands.
     */
    @Test
    public void testGetPostDirectoryName() throws Exception {
        System.out.println("getPostDirectoryName");
        
        assert "".equals(instance.getPostDirectoryName());
    }
    
    /**
     * Test of getPostFileName method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommands.
     */
    @Test
    public void testGetPostFileName() throws Exception {
        System.out.println("getPostFileName");
        
        assert "".equals(instance.getPostFileName());
    }
    
    /**
     * Test of getAppend method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommands.
     */
    @Test
    public void testGetAppend() {
        System.out.println("getAppend");
        
        assert !instance.getAppend();
    }
    
    /**
     * Test of getCleanupOnPutFailure method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommands.
     */
    @Test
    public void testGetCleanupOnPutFailure() {
        System.out.println("getCleanupOnPutFailure");
        
        assert !instance.getCleanupOnPutFailure();
    }
    
    /**
     * Test of setCleanupOnPutFailure method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommands.
     */
    @Test
    public void testSetCleanupOnPutFailure() {
        System.out.println("setCleanupOnPutFailure");
        
        instance.setCleanupOnPutFailure(true);
        assert instance.getCleanupOnPutFailure(); }
    
    /**
     * Test of resolvePostTransfer method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommands.
     */
    @Test
    public void testResolvePostTransfer() throws Exception {
        System.out.println("resolvePostTransfer");
        
        instance.resolvePostTransfer();
    }
    
    /**
     * Test of resolvePreTransfer method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommands.
     */
    @Test
    public void testResolvePreTransfer() throws Exception {
        System.out.println("resolvePreTransfer");
        
        instance.resolvePreTransfer();
    }
    
    /**
     * Test of resolveTargetLocation method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommands.
     */
    @Test
    public void testResolveTargetLocation() throws Exception {
        System.out.println("resolveTargetLocation");
        
        instance.resolveTargetLocation();
    }
}
