/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FtpFileProviderImplTest.java 
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.ftpbc.ftp;

import com.sun.jbi.ftpbc.ftp.exception.FtpFileException;
import com.sun.jbi.ftpbc.ftp.tests.TestUtils;
import org.apache.commons.net.SocketFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileListParser;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketTimeoutException;
import java.util.UUID;

/**
 * JUnit based test.
 *
 * 
 * 
 * 
 * @author Harry Liu (harry.liu@sun.com)
 * @author theyoz
 * Copyright 2006 Sun Microsystems, Inc. All Rights Reserved.
 */
public class FtpFileProviderImplTest extends AbstractFtpTest {
    FtpFileProviderImpl instance;

    @Override
    protected void afterInitializeFtp() throws Exception {
        super.afterInitializeFtp();
        instance = (FtpFileProviderImpl)ftp.getProvider();
    }

    /**
     * Test of listFiles method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testListFiles_files_listRealData() throws Exception {
        System.out.println("listFiles_files_listRealData");
        
        assert instance.listFiles(null, true) == null;

        FTPFile[] files = new FTPFile[1];
        files[0] = new FTPFile();
        assert instance.listFiles(files, true) == null;
        
        assert instance.listFiles(files, false) != null;
    }

    /**
     * Test of listFiles method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testListFiles_pathName() throws Exception {
        System.out.println("listFiles_pathName");

        assert instance.listFiles("") != null;
    }

    /**
     * Test of listFiles method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testListFiles_files_fileType() throws Exception {
        System.out.println("listFiles_files_fileType");

        //FTPFile[] files = null;
        //int fileType = FTPFile.DIRECTORY_TYPE;
        assert instance.listFiles(null, FTPFile.DIRECTORY_TYPE) == null;

        FTPFile[] files = new FTPFile[] {new FTPFile()};
        assert instance.listFiles(files, FTPFile.DIRECTORY_TYPE) == null;
        assert instance.listFiles(files, FTPFile.UNKNOWN_TYPE) != null;
    }

    /**
     * Test of listFiles method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testListFiles_pathName_regExp() throws Exception {
        System.out.println("listFiles_pathName_regExp");

        assert instance.listFiles("", "") != null;

    }

    /**
     * Test of listFiles method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testListFiles() throws Exception {
        System.out.println("listFiles");

        assert instance.listFiles() != null;
    }

    /**
     * Test of appendFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test(expectedExceptions = FileNotFoundException.class)
    public void testAppendFile_remoteDirName_remoteBaseFileName_localFileName() throws Exception {
        System.out.println("appendFile_remoteDirName_remoteBaseFileName_localFileName");

        instance.appendFile("", "", "");
    }

    /**
     * Test of appendFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testAppendFile_remoteDirName_remoteBaseFileName_local() throws Exception {
        System.out.println("appendFile_remoteDirName_remoteBaseFileName_local");

        assert !instance.appendFile("", "", (InputStream)null);
    }

    /**
     * Test of appendFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test(expectedExceptions = FileNotFoundException.class)
    public void testAppendFile_remoteFileName_localFileName() throws Exception {
        System.out.println("appendFile_remoteFileName_localFileName");

        instance.appendFile("", "");
    }

    /**
     * Test of appendFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testAppendFile_remoteFileName_local() throws Exception {
        System.out.println("appendFile_remoteFileName_local");

        assert !instance.appendFile("", (InputStream)null);
    }

    /**
     * Test of appendFileStream method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testAppendFileStream_remoteFileName() throws Exception {
        System.out.println("appendFileStream_remoteFileName");

        assert instance.appendFileStream("") == null;
    }

    /**
     * Test of appendFileStream method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testAppendFileStream_dirName_baseFileName() throws Exception {
        System.out.println("appendFileStream_dirName_baseFileName");

        assert instance.appendFileStream("", "") == null;
    }

    /**
     * Test of deleteFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testDeleteFile_remoteFileName() throws Exception {
        System.out.println("deleteFile_remoteFileName");

        assert !instance.deleteFile("");
    }

    /**
     * Test of deleteFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testDeleteFile_dirName_baseFileName() throws Exception {
        System.out.println("deleteFile_dirName_baseFileName");

        assert !instance.deleteFile("", "");
    }

    /**
     * Test of isTraceRawCommand method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testIsTraceRawCommand() {
        System.out.println("isTraceRawCommand");

        assert !instance.isTraceRawCommand();
    }

    /**
     * Test of rename method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testRename_remoteFileNameFrom_remoteFileNameTo() throws Exception {
        System.out.println("rename_remoteFileNameFrom_remoteFileNameTo");

        assert !instance.rename("", "");
    }

    /**
     * Test of rename method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testRename_dirNameFrom_baseFileNameFrom_dirNameTo_baseFileNameTo() throws Exception {
        System.out.println("rename_dirNameFrom_baseFileNameFrom_dirNameTo_baseFileNameTo");

        assert !instance.rename("", "", "", "");
    }

    /**
     * Test of retrieveFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test(expectedExceptions = FileNotFoundException.class)
    public void testRetrieveFile_remoteDirName_remoteBaseFileName_localFileName() throws Exception {
        System.out.println("retrieveFile_remoteDirName_remoteBaseFileName_localFileName");

        instance.retrieveFile("", "", "");
    }

    /**
     * Test of retrieveFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test(expectedExceptions = FileNotFoundException.class)
    public void testRetrieveFile_remoteFileName_localFileName() throws Exception {
        System.out.println("retrieveFile_remoteFileName_localFileName");

        instance.retrieveFile("", "");
    }

    /**
     * Test of retrieveFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testRetrieveFile_remoteFileName_local() throws Exception {
        System.out.println("retrieveFile_remoteFileName_local");

        assert !instance.retrieveFile("", (OutputStream)null);
    }

    /**
     * Test of retrieveFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testRetrieveFile_remoteDirName_remoteBaseFileName_local() throws Exception {
        System.out.println("retrieveFile_remoteDirName_remoteBaseFileName_local");

        assert !instance.retrieveFile("", "", (OutputStream)null);
    }

    /**
     * Test of retrieveFileStream method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testRetrieveFileStream_remoteFileName() throws Exception {
        System.out.println("retrieveFileStream_remoteFileName");

        assert instance.retrieveFileStream("") == null;
    }

    /**
     * Test of retrieveFileStream method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testRetrieveFileStream_dirName_baseFileName() throws Exception {
        System.out.println("retrieveFileStream_dirName_baseFileName");

        assert instance.retrieveFileStream("", "") == null;
    }

    @DataProvider
    public Object[][] dpSendCommand() {
        return new Object[][] {
                {"CWD", "/", 250},
                {"PWD", null, 257}
        };
    }

    @Test(dataProvider = "dpSendCommand")
    public void testSendCommand(String command, String parm, int expResult) throws Exception {
        int result = parm == null ? instance.sendCommand(command) : instance.sendCommand(command, parm);

        assert expResult == result :
                String.format("Expected result %d but %d returned for [%s][%s]", expResult, result, command, parm);

        assert instance.getReplyCode() == result :
                String.format("Expected getReplyCode() %d but %d returned for [%s][%s]", expResult, result, command, parm);
    }

    /**
     * Test of setTraceRawCommand method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testSetTraceRawCommand() {
        System.out.println("setTraceRawCommand");

        instance.setTraceRawCommand(true);
        assert instance.isTraceRawCommand();
    }

    /**
     * Test of storeFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testStoreFile_remoteFileName_local() throws Exception {
        System.out.println("storeFile_remoteFileName_local");

        assert !instance.storeFile("", (InputStream)null);
    }

    /**
     * Test of storeFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test(expectedExceptions = FileNotFoundException.class)
    public void testStoreFile_remoteFileName_localFileName() throws Exception {
        System.out.println("storeFile_remoteFileName_localFileName");

        instance.storeFile("", "");
    }

    /**
     * Test of storeFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testStoreFile_remoteDirName_remoteBaseFileName_local() throws Exception {
        System.out.println("storeFile_remoteDirName_remoteBaseFileName_local");

        assert !instance.storeFile("", "", (InputStream)null);
    }

    /**
     * Test of storeFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test(expectedExceptions = FileNotFoundException.class)
    public void testStoreFile_remoteDirName_remoteBaseFileName_localFileName() throws Exception {
        System.out.println("storeFile_remoteDirName_remoteBaseFileName_localFileName");

        instance.storeFile("", "", "");
    }

    /**
     * Test of storeFileStream method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testStoreFileStream_dirName_baseFileName() throws Exception {
        System.out.println("storeFileStream_dirName_baseFileName");

        assert instance.storeFileStream("", "") == null;
    }

    /**
     * Test of storeFileStream method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testStoreFileStream_remoteFileName() throws Exception {
        System.out.println("storeFileStream_remoteFileName");

        assert instance.storeFileStream("") == null;
    }

    /**
     * Test of getReplyString method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testGetReplyString() {
        System.out.println("getReplyString");

        assert instance.getReplyString() != null; // remotehelp
    }

    /**
     * Test of getReplyStrings method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testGetReplyStrings() {
        System.out.println("getReplyStrings");

        String[] result = instance.getReplyStrings();
        assert result != null; // remotehelp
        assert result.length > 0; // remotehelp

    }

    /**
     * Test of ascii method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testAscii() throws Exception {
        System.out.println("ascii");

        assert instance.ascii();
    }

    /**
     * Test of binary method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testBinary() throws Exception {
        System.out.println("binary");

        assert instance.binary();
    }

    /**
     * Test of cd method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testCd() throws Exception {
        System.out.println("cd");

        assert instance.cd("/");
    }

    @DataProvider
    public Object[][] dpConnectHostPort() {
        return new Object[][] {
                {"", 0},
                {"localhost", 0},
                {"not.exists.com", 21}
        };
    }

    /**
     * Test of connect method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test(dataProvider = "dpConnectHostPort", expectedExceptions = NullPointerException.class)
    public void testConnectHostPort(String host, int port) throws Exception {
        FtpFileProviderImpl o = new FtpFileProviderImpl();
        o.connect(host, port, 2000);
    }

    /**
     * Test of getDataConnectionMode method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testGetDataConnectionMode() {
        System.out.println("getDataConnectionMode");

        assert instance.getDataConnectionMode() == FTPClient.PASSIVE_LOCAL_DATA_CONNECTION_MODE;
    }

    /**
     * Test of getSystemName method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testGetSystemName() throws Exception {
        System.out.println("getSystemName");

        assert instance.getSystemName() != null;
    }

    /**
     * Test of image method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testImage() throws Exception {
        System.out.println("image");

        assert instance.image();
    }


    /**
     * Test of isConnected method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testIsConnected() {
        System.out.println("isConnected");

        assert instance.isConnected();
    }

    /**
     * Test of isNegativePermanent method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testIsNegativePermanent() {
        System.out.println("isNegativePermanent");

        assert instance.isNegativePermanent(567);
    }

    /**
     * Test of isNegativeTransient method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testIsNegativeTransient() {
        System.out.println("isNegativeTransient");

        assert instance.isNegativeTransient(456);
    }

    /**
     * Test of isPositiveCompletion method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testIsPositiveCompletion() {
        System.out.println("isPositiveCompletion");

        assert instance.isPositiveCompletion(234);
    }

    /**
     * Test of isPositiveIntermediate method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testIsPositiveIntermediate() {
        System.out.println("isPositiveIntermediate");

        assert instance.isPositiveIntermediate(345);
    }

    /**
     * Test of isPositivePreliminary method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testIsPositivePreliminary() {
        System.out.println("isPositivePreliminary");

        assert instance.isPositivePreliminary(123);
    }

    /**
     * Test of listHelp method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testListHelp() throws Exception {
        System.out.println("listHelp");

        assert instance.listHelp() != null;
    }

    /**
     * Test of listHelp method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testListHelp_command() throws Exception{
        System.out.println("listHelp");
        assert instance.listHelp("DELE") != null;
    }

    @DataProvider
    public Object[][] dpLoginFails() {
        return new Object[][] {
                {"", ""},
                {"unknownuser", ""},
                {"unknownuser", "pass"},
                {"", "pass"}
        };
    }

    /**
     * Test of login method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test(dataProvider = "dpLoginFails")
    public void testLoginFails(String user, String password) throws Exception {
        System.out.println("login");

        FtpInterface ftp = new FtpInterface();
        ftp.initialize(this.initializationProperties);
        FtpFileProviderImpl o = (FtpFileProviderImpl)ftp.getProvider();

        o.connect(
                initializationProperties.getProperty(FtpFileConfigConstants.C_P_FTP_HOST),
                Integer.parseInt(initializationProperties.getProperty(FtpFileConfigConstants.C_P_FTP_PORT)));

        assert !o.login(user, password);
    }

    /**
     * Test of logout method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testLogout() throws Exception {
        System.out.println("logout");

        FtpInterface ftp = new FtpInterface();
        ftp.initialize(this.initializationProperties);
        FtpFileProviderImpl o = (FtpFileProviderImpl)ftp.getProvider();

        o.connect(
                initializationProperties.getProperty(FtpFileConfigConstants.C_P_FTP_HOST),
                Integer.parseInt(initializationProperties.getProperty(FtpFileConfigConstants.C_P_FTP_PORT)));

        assert o.login(
                initializationProperties.getProperty(FtpFileConfigConstants.C_P_FTP_USR),
                initializationProperties.getProperty(FtpFileConfigConstants.C_P_FTP_PASSWD));

        assert o.logout();
    }

    /**
     * Test of pwd method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testPwd() throws Exception {
        System.out.println("pwd");

        assert "/".equals(instance.pwd());
    }

    /**
     * Test of sendSiteCommand method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testSendSiteCommand() throws Exception {
        System.out.println("sendSiteCommand");

        assert !instance.sendSiteCommand("site ?");
    }

    /**
     * Test of useActive method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testUseActive() {
        System.out.println("useActive");

        instance.useActive();
    }

    /**
     * Test of usePassive method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testUsePassive() {
        System.out.println("usePassive");

        instance.usePassive();
    }


    /**
     * Test of archiveFile method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testArchiveFile() throws Exception {
        System.out.println("archiveFile");

        assert !instance.archiveFile("", "", "", "");
    }

    /**
     * Test of completePendingCommand method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test(expectedExceptions = SocketTimeoutException.class)
    public void testCompletePendingCommand() throws Exception {
        System.out.println("completePendingCommand");

        int saved = instance.getSoTimeout();

        try {
            instance.setSoTimeout(100); // to prevent from infinite blocking
            instance.listFiles(); // to enable completePendingCommand() to make sense
            instance.completePendingCommand(); // block on timeout
        } finally {
            instance.setSoTimeout(saved);
        }
    }

    /**
     * Test of listFileNames method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testListFileNames_dir_isDirRegex_file_isFileRegex() throws Exception {
        System.out.println("listFileNames_dir_isDirRegex_file_isFileRegex");

        String[] result = instance.listFileNames("", true, "", true);
        assert result != null;
        assert result.length == 0;
    }

    /**
     * Test of listFileNames method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testListFileNames_files() throws Exception {
        System.out.println("listFileNames_files");

        assert instance.listFileNames(null) == null;
    }

    /**
     * Test of getHeuristics method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testGetHeuristics() {
        System.out.println("getHeuristics");

        assert instance.getHeuristics() != null;
    }

    /**
     * Test of mkdir method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testMkdir() throws Exception {
        System.out.println("mkdir");

        assert !instance.mkdir("");
    }

    /**
     * Test of mkdirs method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testMkdirs() throws Exception {
        System.out.println("mkdirs");

        assert instance.mkdirs("/my/" + UUID.randomUUID().toString()) : "no permission";
    }

    /**
     * Test of setSocketFactory method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testSetSocketFactory() {
        System.out.println("setSocketFactory");

       // instance.setSocketFactory(null);
    }

    /**
     * Test of getSoLinger method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testGetSoLinger() throws Exception {
        System.out.println("getSoLinger");

        assert instance.getSoLinger() == -1;
    }

    /**
     * Test of getSoTimeout method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testGetSoTimeout() throws Exception {
        System.out.println("getSoTimeout");

        assert instance.getSoTimeout() == 45000;
    }

    /**
     * Test of getTcpNoDelay method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testGetTcpNoDelay() throws Exception {
        System.out.println("getTcpNoDelay");

        assert !instance.getTcpNoDelay();
    }

    /**
     * Test of isRemoteVerificationEnabled method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testIsRemoteVerificationEnabled() throws Exception {
        System.out.println("isRemoteVerificationEnabled");

        assert instance.isRemoteVerificationEnabled();
    }

    /**
     * Test of setRemoteVerificationEnabled method, of class com.sun.jbi.ftpbc.ftp.FtpFileProviderImpl.
     */
    @Test
    public void testSetRemoteVerificationEnabled() throws Exception {
        System.out.println("setRemoteVerificationEnabled");

        instance.setRemoteVerificationEnabled(true);
        assert instance.isRemoteVerificationEnabled();
    }
}
