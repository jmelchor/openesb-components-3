package com.pymma.encoder.json;


import de.odysseus.staxon.xml.util.PrettyXMLEventWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.Writer;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.transform.stream.StreamSource;
import com.pymma.encoder.json.JSONEncoder;
import org.wiztools.xsdgen.XsdGen;


/**
 * @Company Pymma 
 * @author Aymeric
 */
public class JSONtoXSD {

    /**
     * @param args contains JSON address, root, namespace, XML Name and XSD Name
     */
    
    public static void main(String[] args) throws Exception {
        String JSONAddress = args[0];
        String Root = args[1];
        String namespace = args[2];
        String XMLAddress = args[3];
        String XSDAddress = args[4];
        InputStream input = new FileInputStream(JSONAddress);
        XMLEventWriter writer;
        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        JSONEncoder json = new JSONEncoder(null, null, Root, namespace);
        StreamSource src = (StreamSource)json.decodeFromStream(input);
        String text=src.toString();
        Writer filewriter = new FileWriter(XMLAddress);
        writer = factory.createXMLEventWriter(filewriter);
        writer = new PrettyXMLEventWriter(writer);
        writer.add(XMLInputFactory.newInstance().createXMLEventReader(src.getInputStream()));
        writer.flush();
        writer.close();
        XsdGen gen = new XsdGen();
        gen.parse(new File(XMLAddress));
        File out = new File(XSDAddress);
        gen.write(new FileOutputStream(out));
    }
}
