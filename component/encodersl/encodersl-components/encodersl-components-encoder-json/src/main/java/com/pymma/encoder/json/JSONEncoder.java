/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pymma.encoder.json;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import org.apache.xmlbeans.SchemaGlobalElement;
import org.apache.xmlbeans.XmlException;
import de.odysseus.staxon.json.JsonXMLConfig;
import de.odysseus.staxon.json.JsonXMLConfigBuilder;
import de.odysseus.staxon.json.JsonXMLInputFactory;
import com.sun.encoder.Encoder;
import com.sun.encoder.EncoderConfigurationException;
import com.sun.encoder.EncoderException;
import com.sun.encoder.EncoderProperties;
import com.sun.encoder.EncoderType;
import com.sun.encoder.MetaRef;
import com.pymma.encoder.json.i18n.Messages;
import de.odysseus.staxon.json.JsonXMLOutputFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.commons.io.Charsets;
import org.apache.xmlbeans.SchemaTypeLoader;
import org.apache.xmlbeans.SchemaTypeSystem;
import org.apache.xmlbeans.XmlBeans;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.impl.xb.xsdschema.SchemaDocument;
import org.apache.commons.io.IOUtils;
/**
 *
 * @author Aymeric
 */
public class JSONEncoder implements Encoder {

    private static Messages mMessages = Messages.getMessages(JSONEncoder.class);
    public static final String DEFAULT_ENCODING = "ASCII";

    /**
     * The qualified name of the "source" attribute in the "appinfo" element
     */
    public static final QName APPINFO_SOURCE_ATTR = new QName("source");

    /**
     * The namespace URN that represents the Sun encoder extension to XSD
     */
    public static final String ENCODER_NAMESPACE = "urn:com.pymma:encoder";

    public static final String JSON_ENCODER_NAMESPACE = "urn:com.pymma:encoder-json-1.0";

    public static final String JSON_NS = "urn:json-org:v2xml";

    private final EncoderType mType; // Should never be null

    private final EncoderProperties mProperties; // Should never be null

    // but might be empty
    private Transformer mTransformer;

    private SchemaGlobalElement mRootElement;

    // The pre-decoding charatcer coding read from the metadata (the XSD)
    private String mPreDecodeCharCoding;

    // The post-encoding charatcer coding read from the metadata (the XSD)
    private String mPostEncodeCharCoding;
    private String RootName;
    private URL mSchemaLocation;
    private String mNameRoot= "root";
    private String mNamespace = "namespace";
    /**
     * Logger object.
     */
    private Logger mLog = Logger.getLogger(getClass().getName());
    
    

    JSONEncoder(EncoderType type, EncoderProperties properties) {
        mType = type;
        if (properties.immutable()) {
            mProperties = properties;
        } else {
            mProperties = properties.cloneImmutable();
        }
    }
    public JSONEncoder(EncoderType type, EncoderProperties properties, String root, String namespace) {
        mType = type;
        mNameRoot = root;
        mProperties = properties;
        mNamespace = namespace;
        /*if (properties.immutable()) {
            mProperties = properties;
        } else {
            mProperties = properties.cloneImmutable();
        }*/
    }

    public Source decodeFromString(String input) throws EncoderException {
        EncoderProperties encoderProps = null;
        return decodeFromString(input, encoderProps);
    }

    public String encodeToString(Source src) throws EncoderException {
        EncoderProperties encoderProps = null;
        return encodeToString(src, encoderProps);
    }

    public Source decodeFromBytes(byte[] input) throws EncoderException {
        EncoderProperties encoderProps = null;
        return decodeFromBytes(input, encoderProps);
    }

    public byte[] encodeToBytes(Source src) throws EncoderException {
        EncoderProperties encoderProps = null;
        return encodeToBytes(src, encoderProps);
    }

    public boolean dispose() {
        mRootElement = null;
        mTransformer = null;
        mSchemaLocation = null;
        System.gc();
        if (mLog.isLoggable(Level.FINE)) {
            mLog.fine("Invoked JSONEncoder.dispose() and System.gc().");
        }
        return true;
    }

    /**
     * Sets the xsd schema reference.
     *
     * @param xsd
     * @throws EncoderConfigurationException
     */
    public void setMeta(MetaRef xsd)
            throws EncoderConfigurationException {
        try {
            SchemaTypeSystem schemaTS;
            SchemaTypeLoader typeLoader = XmlBeans.typeLoaderForClassLoader(
                    JSONEncoder.class.getClassLoader());
            String errMsg = null;
            URL xsdurl = xsd.getURL();
            if (xsdurl != null) {
                XmlOptions options = new XmlOptions();
                options.put(XmlOptions.COMPILE_DOWNLOAD_URLS, Boolean.TRUE);
                XmlObject schemaXmlObj = SchemaDocument.Factory.parse(xsdurl);
                schemaTS = XmlBeans.compileXsd(
                        new XmlObject[]{schemaXmlObj},
                        SchemaDocument.type.getTypeSystem(),
                        options);
            } else {
                XmlObject schemaXmlObj = SchemaDocument.Factory.parse(
                        new File(xsd.getPath()));
                schemaXmlObj.validate();
                schemaTS = XmlBeans.compileXsd(
                        new XmlObject[]{schemaXmlObj},
                        SchemaDocument.type.getTypeSystem(),
                        null);
            }
            typeLoader = XmlBeans.typeLoaderUnion(
                    new SchemaTypeLoader[]{typeLoader, schemaTS});
            QName qName = xsd.getRootElemName();
            if (qName == null) {
                errMsg = mMessages.getString("HL7ENC-E2001.Root_Element_is_Null",
                        new Object[]{xsd.getPath()});
                throw new EncoderConfigurationException(errMsg);
            }
            mRootElement = typeLoader.findElement(qName);
            if (mRootElement == null) {
                errMsg = mMessages.getString("HL7ENC-E2002.Global_Element_is_not_found",
                        new Object[]{qName});
                throw new EncoderConfigurationException(errMsg);
            }
            mTransformer = TransformerFactory.newInstance().newTransformer();
            if (xsdurl != null) {
                mSchemaLocation = xsdurl;
            } else {
                mSchemaLocation = new File(xsd.getPath()).toURL();
            }
        } catch (XmlException e) {
            throw new EncoderConfigurationException(e.getMessage(), e);
        } catch (IOException e) {
            throw new EncoderConfigurationException(e.getMessage(), e);
        } catch (TransformerConfigurationException e) {
            throw new EncoderConfigurationException(e.getMessage(), e);
        } catch (TransformerFactoryConfigurationError e) {
            throw new EncoderConfigurationException(e.getMessage(), e);
        }
    }

    public void setMeta(URL schemaLocation, SchemaGlobalElement rootElemName)
            throws EncoderConfigurationException {
        try {
            mRootElement = rootElemName;
            mTransformer = TransformerFactory.newInstance().newTransformer();
            mSchemaLocation = schemaLocation;
        } catch (TransformerConfigurationException e) {
            throw new EncoderConfigurationException(e.getMessage(), e);
        } catch (TransformerFactoryConfigurationError e) {
            throw new EncoderConfigurationException(e.getMessage(), e);
        }
    }

    public EncoderType getType() {
        return mType;
    }

    public EncoderProperties getProperties() {
        return mProperties;
    }

    public Source decodeFromReader(Reader input) throws EncoderException {
        EncoderProperties encoderProps = null;
        return decodeFromReader(input, encoderProps);
    }

    public void encodeToWriter(Source in, Writer out) throws EncoderException {

    }

    public Source decodeFromStream(InputStream input) throws EncoderException {
        return decodeFromStream(input, null);
    }

    public void encodeToStream(Source in, OutputStream out) throws EncoderException {

    }

    public Source decodeFromString(String input, EncoderProperties properties)
            throws EncoderException {
        InputStream inputStream = null;
        inputStream = IOUtils.toInputStream(input, Charsets.UTF_8);
        return decodeFromStream(inputStream, properties);
    }

    public String encodeToString(Source src, EncoderProperties properties)
            throws EncoderException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        encodeToStream(src, out, properties);
        return out.toString();
    }

    public Source decodeFromBytes(byte[] input, EncoderProperties properties)
            throws EncoderException {
        InputStream inputStream = new ByteArrayInputStream(input);
        return decodeFromStream(inputStream, properties);
    }

    public byte[] encodeToBytes(Source src, EncoderProperties properties)
            throws EncoderException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        encodeToStream(src, out, properties);
        ByteArrayOutputStream outArray = (ByteArrayOutputStream) out;
        return outArray.toByteArray();
    }

    public Source decodeFromReader(Reader input, EncoderProperties properties)
            throws EncoderException {
        InputStream inputStream=null;
        try {
            inputStream = IOUtils.toInputStream(IOUtils.toString(input), Charsets.UTF_8);
        } catch (IOException ex) {
            Logger.getLogger(JSONEncoder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return decodeFromStream(inputStream, properties);
    }

    public void encodeToWriter(Source in, Writer out, EncoderProperties properties)
            throws EncoderException {
        ByteArrayOutputStream outstream = new ByteArrayOutputStream();
        encodeToStream(in, outstream, properties);
        out = new OutputStreamWriter(outstream);
    }

    public Source decodeFromStream(InputStream input, EncoderProperties properties)
            throws EncoderException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        JsonXMLConfig config;
        if (!mNameRoot.equals("root")) {
            config = new JsonXMLConfigBuilder().multiplePI(false).autoArray(true).virtualRoot(mNameRoot).build();
        }
        else {
            config = new JsonXMLConfigBuilder().multiplePI(false).autoArray(true).build();
        }
        StreamSource src =null;
        XMLEventReader reader;
        try {
            reader = new JsonXMLInputFactory(config).createXMLEventReader(input);
            if (!mNamespace.equals("namespace")) {
                reader = new NamespaceAddingEventReader(reader, mNamespace);
            }
            XMLEventWriter writer;
            XMLOutputFactory factory = XMLOutputFactory.newInstance();
            writer = factory.createXMLEventWriter(out);
            //writer = new PrettyXMLEventWriter(writer);
            writer.add(reader);
            writer.flush();
            reader.close();
            writer.close();
            
        } catch (XMLStreamException ex) {
            Logger.getLogger(JSONEncoder.class.getName()).log(Level.SEVERE, null, ex);
        }
        ByteArrayOutputStream buffer = out;
        byte[] bytes = buffer.toByteArray();
        InputStream inputStream = new ByteArrayInputStream(bytes);
        src = new StreamSource(inputStream);
        return src;
    }

    public void encodeToStream(Source in, OutputStream out,
            EncoderProperties properties) throws EncoderException {
        StringWriter string = new StringWriter();
        StreamResult result = new StreamResult(string);
        TransformerFactory tFactory =TransformerFactory.newInstance();
        Transformer transformer;
        JsonXMLConfig config = new JsonXMLConfigBuilder().autoArray(true).autoPrimitive(true).namespaceDeclarations(false).prettyPrint(true).build();
        if (out==null)
        {
            out = new ByteArrayOutputStream();
        }
        try {
            transformer = tFactory.newTransformer();
            transformer.setOutputProperty("indent","yes");
            transformer.transform(in, result); 
            //String replace = string.toString().replaceAll("<([/a-z0-9]*):","<");
            InputStream stream = IOUtils.toInputStream(string.toString(), "UTF-8");
            
            XMLEventReader reader = XMLInputFactory.newInstance().createXMLEventReader(stream);
            /*
             * Create writer (JSON).
             */
            ByteArrayOutputStream mid = new ByteArrayOutputStream();
            //XMLEventWriter writer = new JsonXMLOutputFactory(config).createXMLEventWriter(out);
            
            XMLEventWriter writer = new JsonXMLOutputFactory(config).createXMLEventWriter(mid);
            /*
             * Copy events from reader to writer.
             */
            writer.add(reader);
            /*
             * Close reader/writer.
             */
            reader.close();
            writer.close();
            String convert = new String(mid.toByteArray(), "UTF-8");
            convert = convert.replaceAll("\"([/a-z0-9])*:","\"");
            out.write(convert.getBytes());
            
        } catch (XMLStreamException ex) {
            Logger.getLogger(JSONEncoder.class.getName()).log(Level.SEVERE, null, ex);
        }
         catch (TransformerConfigurationException ex) {
            Logger.getLogger(JSONEncoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(JSONEncoder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JSONEncoder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        /*InputStream input = new FileInputStream("C:/Users/Aymeric/Desktop/gsontest.txt");
        XMLEventWriter writer;
        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        JSONEncoder json = new JSONEncoder(null, null, "Test", null);
        StreamSource src = (StreamSource)json.decodeFromStream(input);
        String text=src.toString();
        Writer filewriter = new FileWriter("/Users/Aymeric/Desktop/xmlresult.xml");
        writer = factory.createXMLEventWriter(filewriter);
        writer = new PrettyXMLEventWriter(writer);
        
        writer.add(XMLInputFactory.newInstance().createXMLEventReader(src.getInputStream()));
        writer.flush();
        
        writer.close();*/
    }

}
